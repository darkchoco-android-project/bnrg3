package kr.cloudscape.android.criminalintent;

import java.util.Date;
import java.util.UUID;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(prefix = "m")
public class Crime {

    @Getter private UUID mId;
    @Getter @Setter private String mTitle;
    @Getter @Setter private Date mDate;
    @Getter @Setter boolean mSolved;

    public Crime() {
        mId = UUID.randomUUID();
        mDate = new Date();
    }
}
